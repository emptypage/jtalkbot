FROM ubuntu:22.04
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3.10 python3.10-venv python3.10-dev libffi-dev libopus0 \
    open-jtalk open-jtalk-mecab-naist-jdic hts-voice-nitech-jp-atr503-m001 \
    && apt-get -y clean && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/jtalkbot
COPY dist dist
RUN python3.10 -m venv venv \
    && venv/bin/python -m pip install --pre -f dist jtalkbot
ENV PATH /opt/jtalkbot/venv/bin:$PATH

WORKDIR /srv/jtalkbot
RUN groupadd -g 61000 jtalkbot \
    && useradd -u 61000 -g jtalkbot -m -s /bin/bash jtalkbot \
    && chown jtalkbot:jtalkbot .
USER jtalkbot:jtalkbot
