.PHONY: check_git_status build clean testpypi pypi docker docker_hub cleanall version

NAME = jtalkbot
VERSION = `python -m setuptools_scm`
DOCKER_HUB_USER = emptypage

check_git_status:
	@test -z "`git status -s`" || (echo "Repository is not clean" && exit 1)

clean:
	-rm dist/*

build: clean
	python -m build

testpypi: check_git_status build
	twine upload -r testpypi --skip-existing dist/*

pypi: check_git_status build
	twine upload dist/*

docker: build
	docker build --tag $(NAME) .

docker_hub: check_git_status build
	docker buildx build --no-cache --platform linux/amd64,linux/arm64 \
	-t $(DOCKER_HUB_USER)/$(NAME):$(VERSION) \
	-t $(DOCKER_HUB_USER)/$(NAME):latest \
	--push .

cleanall:
	-rm -r src/jtalkbot/__pycache__
	-rm -r src/jtalkbot.egg-info
	-rm -r dist

version:
	@echo $(VERSION)
